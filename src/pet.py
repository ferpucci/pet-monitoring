from prometheus_client import start_http_server, Counter, Gauge, Info
from time import sleep
import random

# probabilidades de eventos
PROB_HEARTRATE = 0.5
PROB_BODYTEMPERATURE = 0.5
PROB_SLEEP = 0.03
PROB_MEOWING = 0.05
PROB_HANGOUT = 0.03
PROB_EAT = 0.03
PROB_DRINK = 0.03
PROB_BATHROOM = 0.03
PROB_POOP = 0.3
PROB_RECIPIENTFILL = 0.04

class PetSimulation:
    def __init__(self):
        self.number_of_pets = 0
        self.pets = []
        self.number_of_recipientes = 0
        self.recipientes = []

        # pet metrics
        self.heart_gauge = Gauge(
            "batimento_cardiaco_pet",
            "nivel do batimento cardiaco do animal",
            ["nome"],
        )

        self.temperature_gauge = Gauge(
            "temperatura_corporal_pet",
            "temperatura do animal em celcius",
            ["nome"],
        )

        self.sleeping_gauge = Gauge(
            "pet_adormecido",
            "numero identifica se pet esta dormindo ou nao (1 ou 0)",
            ["nome"],
        )

        self.meowing_counter = Counter(
            "contador_miados",
            "contador de miados do pet",
            ["nome"],
        )

        self.meowing = Info(
            "captura_miado",
            "captura textual do miado",
            ["nome"],
        )

        self.petout_gauge = Gauge(
            "pet_fora",
            "numero identifica se pet esta fora de casa ou nao(1 ou 0)",
            ["nome"],
        )

        self.petout_counter = Counter(
            "pet_fora_contador",
            "quantas vezes o pet saiu",
            ["nome"],
        )

        self.bathroom_counter = Counter(
            "pet_banheiro_contador",
            "quantas vezes o pet foi ao banheiro",
            ["nome"],
        )

        self.peteat_counter = Counter(
            "pet_refeicao_contador",
            "quantas vezes o pet fez uma refeicao",
            ["nome"],
        )

        self.petdrink_counter = Counter(
            "pet_bebeuagua_contador",
            "quantas vezes o pet foi bebeu agua",
            ["nome"],
        )

        self.recipient_gauge = Gauge(
            "recipiente_conteudo",
            "Barra de conteudo do recipiente",
            ["id"]
        )

        self.recipientpollution_gauge = Gauge(
            "poluicao_recipiente",
            "Qualidade do conteudo recipiente",
            ["id"]
        )

        self.recipienthumidity_gauge = Gauge(
            "humidade_recipiente",
            "Umidade do recipiente",
            ["id"]
        )
        return

    def add_pet(self, name, age, heart_rate, body_temperature):
        self.pets.append(Pet(name, age, heart_rate,body_temperature))
        self.number_of_pets += 1
        return

    def add_recipient(self, recipient_type, id, info, value):
        if recipient_type == 'agua':
            self.recipientes.append(RecipienteAgua(id, info, value))
        elif recipient_type == 'comida':
            self.recipientes.append(RecipienteComida(id, info, value))
        elif recipient_type == 'areia':
            self.recipientes.append(RecipienteAreia(id, info, value))
        else:
            raise ValueError('Tipo de recipiente nao encontrado')

        self.number_of_recipientes += 1
        return

    def run(self):
        while True:
            sleep(15)
            for pet in self.pets:
                self.simulate_pet(pet)
            for recipient in self.recipientes:
                self.simulate_recipient(recipient)


    # simulacao de comportamento do pet
    def simulate_pet(self, pet):
        # batimento cardiaco
        if pet.heart_rate < 140:
            pet.increase_heartrate(1)
        if pet.heart_rate > 220:
            pet.increase_heartrate(-1)
        if random.random() < PROB_HEARTRATE:
            pet.increase_heartrate(1)
        else:
            pet.increase_heartrate(-1)
        # instrumentacao
        self.heart_gauge.labels(pet.name).set(pet.heart_rate)

        # temperatura corporal
        if pet.body_temperature < 36:
            pet.increase_bodytemperature(0.1)
        if pet.body_temperature > 39:
            pet.increase_bodytemperature(-0.1)
        if random.random() < PROB_BODYTEMPERATURE:
            pet.increase_bodytemperature(0.1)
        else:
            pet.increase_bodytemperature(-0.1)
        # instrumentacao
        self.temperature_gauge.labels(pet.name).set(pet.body_temperature)

        # sleep
        if random.random() < PROB_SLEEP:
            sleep = pet.update_sleep()
            # instrumentacao
            self.sleeping_gauge.labels(pet.name).set(sleep)

        # meow
        meow_probability = 0
        if random.random() < PROB_MEOWING:
            meow_probability = random.random()
            if meow_probability < 0.2:
                pet.update_meow("meowuin")
            elif meow_probability >= 0.2 and meow_probability < 0.4:
                pet.update_meow("MEEEEEEOWWWW")
            elif meow_probability >= 0.4 and meow_probability < 0.7:
                pet.update_meow("meh")
            elif meow_probability >= 0.7 and meow_probability < 0.8:
                pet.update_meow("Ontem sonhei que era uma borboleta que sonhava ser um gato.")
            elif meow_probability >= 0.8 and meow_probability <= 1:
                pet.update_meow("auau")
            # instrumentacao
            self.meowing.labels(pet.name).info({"miado": pet.last_meowing})
            self.meowing_counter.labels(pet.name).inc()

        # hangout
        if random.random() < PROB_HANGOUT:
            hangout = pet.update_hangout()
            # instrumentacao
            self.petout_gauge.labels(pet.name).set(hangout)
            self.petout_counter.labels(pet.name).inc()

        # eating
        if random.random() < PROB_EAT:
            recipiente_comida = random.choice([r for r in self.recipientes if isinstance(r, RecipienteComida)])
            pet.eat()
            recipiente_comida.dec(10)
            # instrumentacao
            self.peteat_counter.labels(pet.name).inc()
            self.recipient_gauge.labels(recipiente_comida.id).set(recipiente_comida.value)

        # drinking
        if random.random() < PROB_DRINK:
            recipiente_agua = random.choice([r for r in self.recipientes if isinstance(r, RecipienteAgua)])
            pet.drink()
            recipiente_agua.dec(10)
            # instrumentacao
            self.petdrink_counter.labels(pet.name).inc()
            self.recipient_gauge.labels(recipiente_agua.id).set(recipiente_agua.value)
            self.recipientpollution_gauge.labels(recipiente_agua.id).set(recipiente_agua.pollution)

        # bathroom
        if random.random() < PROB_BATHROOM:
            recipiente_areia = random.choice([r for r in self.recipientes if isinstance(r, RecipienteAreia)])
            pet.usebathroom()
            if random.random() < PROB_POOP:
                recipiente_areia.update_pollution(10)
                recipiente_areia.update_humidity(10)
            else:
                recipiente_areia.update_pollution(15)
                recipiente_areia.update_humidity(5)
            # instrumentacao
            self.bathroom_counter.labels(pet.name).inc()
            self.recipient_gauge.labels(recipiente_areia.id).set(recipiente_areia.value)
            self.recipienthumidity_gauge.labels(recipiente_areia.id).set(recipiente_areia.humidity)
            self.recipientpollution_gauge.labels(recipiente_areia.id).set(recipiente_areia.pollution)


    def simulate_recipient(self, recipient):
        if random.random() > PROB_RECIPIENTFILL:
            if isinstance(recipient, RecipienteAreia):
                if recipient.pollution > 20 or recipient.humidity > 20 or recipient.value < 20:
                    recipient.update_recipient(100, 0, 0)
                    # instrumentacao
                    self.recipient_gauge.labels(recipient.id).set(recipient.value)
                    self.recipienthumidity_gauge.labels(recipient.id).set(recipient.humidity)
                    self.recipientpollution_gauge.labels(recipient.id).set(recipient.pollution)

            if isinstance(recipient, RecipienteComida):
                if recipient.value < 20:
                    recipient.update_recipient(100)
                    # instrumentacao
                    self.recipient_gauge.labels(recipient.id).set(recipient.value)
            if isinstance(recipient, RecipienteAgua):
                if recipient.pollution > 20 or recipient.value < 20:
                    recipient.update_recipient(100, 0)
                    # instrumentacao
                    self.recipient_gauge.labels(recipient.id).set(recipient.value)
                    self.recipientpollution_gauge.labels(recipient.id).set(recipient.pollution)

class Pet:
    def __init__(self, name: str, age: int,
                 heart_rate: int, body_temperature: float):
        self.name = name
        self.age = age
        self.heart_rate = heart_rate
        self.body_temperature = body_temperature
        self.isSleeping = False
        self.meowing_count = 0
        self.last_meowing = None

        self.isOut = False
        self.hangout_count = 0
        self.bathroom_count = 0
        self.eating_count = 0
        self.drinking_count = 0

    def increase_heartrate(self, value:int):
        self.heart_rate += value

    def increase_bodytemperature(self, value:float):
        self.body_temperature += value

    def update_sleep(self):
        if self.isSleeping == True:
            self.isSleeping = False
            return 0
        else:
            self.isSleeping = True
            return 1

    def update_meow(self, meow: str):
        self.last_meowing = meow
        self.meowing_count += 1

    def update_hangout(self):
        if self.isOut == True:
            self.hangout_count +=1
            self.isOut = False
            return 0
        else:
            self.isOut = True
            return 1

    def eat(self):
        self.eating_count += 1

    def drink(self):
        self.drinking_count += 1

    def usebathroom(self):
        self.bathroom_count += 1

class Recipiente:
    def __init__(self, id: str, info: str, value: float):
        self.id = id
        self.info = info
        self.value = value

    def dec(self, value):
        self.value -= value

    def update_recipient(self):
        pass

class RecipienteAgua(Recipiente):
    def __init__(self, id: str, info: str, value:float):
        super().__init__(id, info, value)
        self.pollution = 0

    def dec(self, value):
        self.value -= value
        self.pollution += 5

    def update_recipient(self, water_level, pollution=None):
        self.value = water_level
        if pollution != None:
            self.pollution = pollution

    def update_pollution(self, pollution):
        if self.pollution < 100:
            self.pollution += pollution


class RecipienteComida(Recipiente):
    def __init__(self, id: str, info: str, value:float):
        super().__init__(id, info, value)

    def update_recipient(self, food_level):
        self.value = food_level

class RecipienteAreia(Recipiente):
    def __init__(self, id: str, info: str, value:float):
        super().__init__(id, info, value)
        self.pollution = 0
        self.humidity = 0

    def update_recipient(self, sand_level,
                         pollution, humidity):
        self.value = sand_level
        self.pollution = pollution
        self.humidity = humidity

    def update_pollution(self, pollution):
        if self.pollution < 100:
            self.pollution += pollution

    def update_humidity(self, humidity):
        if self.humidity < 100:
            self.humidity += humidity






