FROM python:3.8

COPY . /pet_status
WORKDIR ./pet_status
RUN pip install -r requirements.txt

CMD ["python", "main.py"]