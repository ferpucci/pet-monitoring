from src.pet import PetSimulation, Pet
from prometheus_client import start_http_server, Counter, Gauge
from time import sleep

def to_vivao(number_of_calls_counter):
    print("TO VIVAO", flush=True)
    number_of_calls_counter.inc()

if __name__ == "__main__":

    counter = Counter(
        "number_of_calls_counter",
        "Numero de vezes que a funcao to vivo foi exec"
    )

    start_http_server(8080)

    sim = PetSimulation()
    sim.add_pet('miauster sorocabinha', 5, 90, 38.2)
    sim.add_pet('a gata christie', 2, 80, 37.3)
    sim.add_recipient('agua', 'agua', 'recipiente de agua', 100)
    sim.add_recipient('comida', 'comida', 'recipiente de comida', 100)
    sim.add_recipient('areia', 'areia', 'recipiente de comida', 100)
    sim.run()
    pet = Pet(name='rodrigo', age=10)
    print(pet.name, flush=True)
    print("adsasdasdasd", flush=True)
